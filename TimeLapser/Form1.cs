﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace TimeLapser
{
    public partial class Form1 : Form
    {
        public string Status = "Stopped";
        public int PicturesTaken = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void applicationIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Visible = true;
            Activate();
        }

        private void applicationIcon_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog browseDialog = new FolderBrowserDialog();
            browseDialog.Description = "Select output folder";
            browseDialog.RootFolder = Environment.SpecialFolder.MyComputer;
            browseDialog.ShowNewFolderButton = true;
            if (browseDialog.ShowDialog() == DialogResult.OK) txtOutput.Text = browseDialog.SelectedPath;
        }

        private void btnToggle_Click(object sender, EventArgs e)
        {
            Toggle();
        }

        private void Toggle()
        {
            if (Status == "Stopped")
            {
                if (!Directory.Exists(txtOutput.Text))
                {
                    MessageBox.Show("Invalid output!");
                    return;
                }

                Status = "Started";
                btnToggle.Text = "Stop capture";
                txtOutput.Enabled = false;
                startCaptureToolStripMenuItem.Text = "Stop capture";
                applicationIcon.Icon = Properties.Resources.clock_play;
                Visible = false;
                intervalTimer.Interval = (int)interval.Value * 1000;
                intervalTimer.Start();
            }
            else
            {
                Status = "Stopped";
                btnToggle.Text = "Start capture";
                txtOutput.Enabled = true;
                startCaptureToolStripMenuItem.Text = "Start capture";
                applicationIcon.Icon = Properties.Resources.clock_stop;
                intervalTimer.Stop();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Status == "Stopped") return;

            e.Cancel = true;
            Visible = false;
        }

        private void intervalTimer_Tick(object sender, EventArgs e)
        {
            PicturesTaken++;
            lblCount.Text = ""+PicturesTaken;
            applicationIcon.Text = "TimeLapser - " + PicturesTaken + " pictures taken";

            try
            {
                Graphics myGraphics = CreateGraphics();
                Bitmap memoryImage = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, myGraphics);
                Graphics memoryGraphics = Graphics.FromImage(memoryImage);
                memoryGraphics.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y, 0, 0, Screen.PrimaryScreen.Bounds.Size, CopyPixelOperation.SourceCopy);

                string filename = txtOutput.Text + "/" + string.Format("{0:yyyy-MM-dd_hh-mm-ss}.png", DateTime.Now);
                memoryImage.Save(filename, ImageFormat.Png);

                myGraphics.Dispose();
                memoryImage.Dispose();
                memoryGraphics.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show("An error occured:\n" + ex.Message);
                Status = "Stopped";
                btnToggle.Text = "Start capture";
                txtOutput.Enabled = true;
                startCaptureToolStripMenuItem.Text = "Start capture";
                applicationIcon.Icon = Properties.Resources.clock_red;
                intervalTimer.Stop();
            }
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Application.MessageLoop)
            {
                Application.Exit();
            }
            else
            {
                Environment.Exit(1);
            }
        }

        private void startCaptureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Toggle();
        }
    }
}
